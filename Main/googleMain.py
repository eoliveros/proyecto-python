"""
Main class which execute the test project.
@author: Emiliano Oliveros
@since: 08/04/2020
"""

import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.webdriver import WebDriver
from POM import googleSearchPage as gsp


class GoogleTest(unittest.TestCase):

    def test_main(self):
        try:
            # ask in console to the user what to search.
            searchGoogle = input("WHAT DO YOU WANT TO SEARCH IN GOOGLE:")
            driver: WebDriver = webdriver.Chrome('C:\\Users\\eoliveros\\Documents\\Selenium\\chromedriver.exe')
            driver.get("https://www.google.com/")

            driver.maximize_window()
            driver.find_element_by_name("q").send_keys(searchGoogle)
            driver.find_element_by_name("q").send_keys(Keys.ENTER)

            # google home page & search page instance
            googleSearchPage = gsp.GoogleSearchPage(driver)
            self.assertNotEqual(googleSearchPage, None)

            # calling method getList from googleSearchPage POM.
            results = googleSearchPage.getList()
            print(results)
            self.assertIsInstance(results, list)

            # calling method validateSearch from googleSearchPage POM.
            results = googleSearchPage.validateSearchWord()
            print("¿TODAS LAS BÚSQUEDAS TIENEN LA PALABRA BUSCADA?: ", results)
            

            # calling method validateDescription from googleSearchPage POM.
            results = googleSearchPage.validateDescription()
            print("¿LOS RESULTADOS TIENE DESCRIPCIÓN?: ", results)

            # calling method validateCorrection from googleSearchPage POM.
            results = googleSearchPage.validateCorrection()
            print(results)

            time.sleep(10)
            driver.quit()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    unittest.main()

