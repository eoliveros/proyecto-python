"""
POM class of the search/results page from goolge used to validate
the results after doing a search.
@autor: Emiliano Oliveros
@since: 08/04/2020
"""
import ec as ec
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


class GoogleSearchPage:

    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.resultsList = None
        self.xpathResults = "//h3[@class='LC20lb DKV0Md']"
        self.xpathValidate = "//input[@class='gLFyf gsfi']"
        self.xpathTitle = "//h3[@class='LC20lb DKV0Md']"
        self.xpathDescription = "//span[@class='st']"
        self.xpathCorrection = "//p[@class='p64x9c card-section']"
        self.titles = []
        self.descriptions = []

    """This method gets the results of the search page and show them in a list"""

    def getList(self):
        wait = WebDriverWait(self.driver, 60)
        self.resultsList = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpathResults)))
        data = []
        for item in self.resultsList:
            data.append(item.text)

        return data

    """ This method validates that the results contains at least one word of the searched word"""

    def validateSearchWord(self):

        search = self.driver.find_element_by_xpath(self.xpathValidate).get_attribute('value')

        for x in self.titles:
            if search in x.text:
                return True
        return False

    """ This method validates that the results contains description"""

    def validateDescription(self):
        self.description = self.driver.find_elements_by_xpath(self.xpathDescription)
        self.title = self.driver.find_elements_by_xpath(self.xpathTitle)
        descriptionSize = len(self.description)
        titleSize = len(self.title)

        if descriptionSize == titleSize:
            return True
        else:
            return False

    def validateCorrection(self):

        self.wait.until(ec.presence_of_all_elements_located((By.XPATH, self.xpathTitle)))
        try:
            self.driver.find_element_by_xpath(self.xpathCorrection)
            aux_flag = self.click_on_suggestion()
            print("Quizás quisiste decir", aux_flag)
        except NoSuchElementException:
            print("NO HAY CORRECIÓN")
            return True
